<%-- 
    Document   : GruposJesus
    Created on : 26-abr-2015, 18:55:06
    Author     : Jelt
--%>

<!DOCTYPE html>
<html ng-app="grupos">
    <head>
        <title>Asistencia</title>
        <meta charset="UTF-9">
        <script src="lib/angular.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/controllers/controllers.js" type="text/javascript"></script>

        
    </head>
    <body ng-controller="grupoCtrl">        

        <br>

        <pre>
       Marca:<input type="text" ng-model="grupo.marca" /><br>
       Procesador:<input type="text" ng-model="grupo.procesador" /><br>
       Ram:<input type="text" ng-model="grupo.ram" />
        
       <button ng-click="agregar(grupo)"><b>Agregar</b></button>
        </pre>        

        <table>
           <thead>
                <tr>
                    <th>Marca:</th>
                    <th>Procesador:</th>
                    <th>Ram</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="grupo in grupos">
                    <td>{{grupo.marca}}</td>
                    <td>{{grupo.procesador}}</td>
                    <td>{{grupo.ram}}</td>
                </tr>
            </tbody>
        </table>


    </body>
</html>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="grupo">
    <head>
        <script src="lib/angular.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/controllers/controllers.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulario en angular</title>
    </head>
    <body ng-controller="grupoCtrl">
        <h1>Taller Salgado (formulario)</h1>
        <pre>
      <b>       MARCA:</b><input type="text" ng-model="grupo.marca" /><br>
        <b>PROCESADOR:</b><input type="text" ng-model="grupo.cpu" /> <br>
        <b>       RAM:</b><input type="text" ng-model="grupo.ram" /> <br> <br>
        <button ng-click="agregar(grupo)">Agregar</button>
        </pre>        

        <br>
        <br>
        <table >
            <thead>
                <tr>
                    <th>Marca</th>
                    <th>Procesador</th>
                    <th> RAM</th>
                </tr>
            </thead>
            <tbody>
                <tr  ng-repeat="grupo in grupos">
                    <td>{{grupo.marca}}</td>
                    <td>{{grupo.cpu}}</td>
                    <td>{{grupo.ram}}</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>

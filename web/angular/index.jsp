<!DOCTYPE html>
<html ng-app="grupos">
    <head>
        <title>Asistencia</title>
        <meta charset="UTF-8">
        
        <script src="../lib/angular.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/controllers/controllers.js" type="text/javascript"></script>

    </head>
    <body ng-controller="grupoCtrl">
        <h3>Grupos</h3>
        <table>
            <thead>
                <tr>
                    <th>Grupo</th>
                    <th>Semestre</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="grupo in grupos">
                    <td>{{grupo.nombre}}</td>
                    <td>{{grupo.semestre}}</td>
                </tr>
            </tbody>
        </table>
        <div style="width: 25%">
        <input type="text" ng-model="grupo.nombre" />
        <input type="text" ng-model="grupo.semestre" />
        <button ng-click="agregar(grupo)">Agregar</button>
        </div>
    </body>
</html>

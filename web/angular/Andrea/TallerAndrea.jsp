
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="grupo">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <script src="lib/angular.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/controllers/controllers.js" type="text/javascript"></script>
    </head>
    <body ng-controller="grupoCtrl">
        <h2>Registrar datos del pc</h2>
          Marca: <input type="text" ng-model="grupo.marca" /><br><br>
        Procesador: <input type="text" ng-model="grupo.cpu" /> <br><br>
        RAM: <input type="text" ng-model="grupo.ram" /> <br> <br><br>
        <button ng-click="agregar(grupo)">Agregar</button><br><br>
        <br>
        <br>
        <table >
            <thead>
                <tr>
                    <th>Marca</th>
                    <th>Procesador</th>
                    <th>RAM</th>
                </tr>
            </thead>
            <tbody>
                <tr  ng-repeat="grupo in grupos">
                    <td>{{grupo.marca}} </td>
                    <td>{{grupo.cpu}}</td>
                    <td>{{grupo.ram}}</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>

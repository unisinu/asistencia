// registrando un controlador para el modulo grupos.
grupos.controller("grupoCtrl", function ($scope, $http) {
    $http.get('/asistencia/angular/grupo').success(function (data) {
        console.info(data)
        $scope.grupos = data; // este es el array de nombres recuperado del servidor
    });

    $scope.agregar = function (grupo) {
        $scope.grupos.push(angular.copy(grupo));
        delete $scope.grupo;

        $http.post('/asistencia/angular/grupo/guardar').
            success(function (data, status, headers, config) {
                console.info("ufff, llego...");
                console.info(data);
                console.info(status);
            }).
            error(function (data, status, headers, config) {
                console.info("naranjas, no llego...");
            });
    }
});
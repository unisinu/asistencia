<!DOCTYPE html>
<html ng-app="grupos">
    <head>
        <title>Asistencia</title>
        <meta charset="UTF-8">
        <script src="../js/angular.js" type="text/javascript"></script>
        <script>
            // creando modulo para asistencias
            var grupos = angular.module("grupos", []);
            // registrando un controlador para el modulo grupos.
            grupos.controller("grupoCtrl", function($scope){
                $scope.grupos = [
                    {
                        nombre : 'Electiva Tecnica',
                        semestre : 3
                    },
                    {
                        nombre : 'Estructura de Datos',
                        semestre : 3
                    },
                    {
                        nombre : 'Electiva interdisciplinar II',
                        semestre : 8
                    }
                ];
                $scope.agregar = function (grupo) {
                    $scope.grupos.push(angular.copy(grupo));
                    delete $scope.grupo;
                }
            });
        </script>

    </head>
    <body ng-controller="grupoCtrl">
        <h3>Grupos</h3>
        <table>
            <thead>
                <tr>
                    <th>Grupo</th>
                    <th>Semestre</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="grupo in grupos">
                    <td>{{grupo.nombre}}</td>
                    <td>{{grupo.semestre}}</td>
                </tr>
            </tbody>
        </table>
        <input type="text" ng-model="grupo.nombre" />
        <input type="text" ng-model="grupo.semestre" />
        <button ng-click="agregar(grupo)">Agregar</button>
    </body>
</html>

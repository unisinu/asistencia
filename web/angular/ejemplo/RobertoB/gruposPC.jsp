<!DOCTYPE html>
<html ng-app="grupos">
    <head>
        <title>Asistencia</title>
        <meta charset="UTF-8">
        <script src="lib/angular.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/controllers/controllers.js" type="text/javascript"></script>

        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: center;
            }
            table#t01 {
                width: 100%;    
                background-color: #f1f1c1;
            }
        </style>
    </head>
    <body ng-controller="grupoCtrl">        

        <br>

        <pre>
          <b>     MARCA:</b><input type="text" ng-model="grupo.marca" /><br>
          <b>PROCESADOR:</b><input type="text" ng-model="grupo.procesador" /><br>
          <b>       RAM:</b><input type="text" ng-model="grupo.ram" />
        
                     <button ng-click="agregar(grupo)"><b>Agregar</b></button>
        </pre>        

        <table id="t01" style="width:25%" >
            <thead>
                <tr>
                    <th>MARCA</th>
                    <th>PROCESADOR</th>
                    <th>RAM</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="grupo in grupos">
                    <td>{{grupo.marca}}</td>
                    <td>{{grupo.procesador}}</td>
                    <td>{{grupo.ram}}</td>
                </tr>
            </tbody>
        </table>


    </body>
</html>

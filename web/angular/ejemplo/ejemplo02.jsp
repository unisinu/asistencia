<!DOCTYPE html>
<html ng-app>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="../lib/angular.js" type="text/javascript"></script>
    </head>
    <body>
        Check me to make text readonly:
        <input type="checkbox" ng-model="checked"><br/>
        <input type="text" 
               ng-readonly="checked" 
               value="I'm Angular"/>
    </body>
</html>

<html ng-app="grupos">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="lib/angular.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/controllers/controllers.js" type="text/javascript"></script>
        <title>Ejercicio</title>
        
         <style>
            table, th, td {
                border: 3px solid ;
                
            }
            th, td {
                padding: 2px;
               
            }
            table#t01 {
                width: 100%;    
                background-color:  #5858FA;
            }
        </style>
        
        
    </head>
    
    <body ng-controller="grupoCtrl">
       <br>

        
          <b>MARCA:</b><input type="text" ng-model="grupo.marca"  size="20" /><br>
          <b>PROCESADOR:</b><input type="text" ng-model="grupo.procesador" size="13"/><br>
          <b>RAM:</b><input type="text" ng-model="grupo.ram" size="24"/>
          <br/>
           <br/>
        
              <button ng-click="agregar(grupo)"><b>Agregar</b></button>
               

        <table id="t01" style="width:25%">
            <thead>
                <tr>
                    <th>MARCA</th>
                    <th>PROCESADOR</th>
                    <th>RAM</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="grupo in grupos">
                    <td>{{grupo.marca}}</td>
                    <td>{{grupo.procesador}}</td>
                    <td>{{grupo.ram}}</td>
                </tr>
            </tbody>
        </table>

    </body>
</html>

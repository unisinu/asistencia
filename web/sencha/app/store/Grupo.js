Ext.define('asistencia.store.Grupo', {
    extend: 'Ext.data.Store',
    fields: ['id','nombre', 'semestre','Aula_id','Asignatura_id','Asistencia_id'],
    model: 'asistencia.model.Grupo',
   autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'http://localhost:8080/asistencia/sencha/asignaturas',
        reader: {
            type: 'json',
            root: 'asignaturas',
            successProperty: 'success'
        }
    }
});
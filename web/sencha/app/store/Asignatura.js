Ext.define('asistencia.store.Asignatura', {
    extend: 'Ext.data.Store',
    fields: ['nombre', 'semestre'],
    model: 'asistencia.model.Asignatura',

   autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'http://localhost:8080/asistencia/sencha/asignaturas',
        reader: {
            type: 'json',
            //root: 'asignaturas',
            successProperty: 'success'
        }
    }
});
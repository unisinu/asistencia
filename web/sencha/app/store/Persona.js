Ext.define('asistencia.store.Persona', {
    extend: 'Ext.data.Store',
    fields: ['codigo', 'nombre','apellido','email','telefono'],
    model: 'asistencia.model.Persona',
    /*
    data: [
        {
            nombre: 'Electiva Tecnica',
            semestre: '3'
        },
        {
            nombre: 'Estructura de Datos',
            semestre: '3'
        }
    ]
    */
   autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'http://localhost:8080/asistencia/sencha/personas',
        reader: {
            type: 'json',
            root: 'asignaturas',
            successProperty: 'success'
        }
    }
});
Ext.define('asistencia.store.Aulas', {
    extend: 'Ext.data.Store',
    fields: ['id', 'nombre'],
    model: 'asistencia.model.Aulas',
    data: [
        { id: '1', nombre: 'Sala desarrollo de software'},
        { id: '2', nombre: 'Sala de redes'}
    ]
});


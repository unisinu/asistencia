Ext.define('asistencia.controller.Grupo', {
    extend: 'Ext.app.Controller',
    views: [
        'grupo.Listado',
        'grupo.Editar',
    ],
    stores: [
        'Grupo'
    ],
    model: [
        'Grupo'
    ],
    init: function () {
        this.control({
            'grupos': {
                itemdblclick: this.editarGrupo
            },
            'editarGrupo button[action=save]': {
                click: this.actualizarGrupo
            }
        });
    },
    editarGrupo: function (grid, record) {
        var view = Ext.widget('editarGrupo');
        view.down('form').loadRecord(record);
    },
    actualizarGrupo: function (button) {
        var win = button.up('window'),
                form = win.down('form'),
                record = form.getRecord(),
                values = form.getValues();

        record.set(values);
        win.close();
    }
});
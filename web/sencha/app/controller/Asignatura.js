Ext.define('asistencia.controller.Asignatura', {
    extend: 'Ext.app.Controller',
    views: [
        'asignatura.Listado',
        'asignatura.Editar',
    ],
    stores: [
        'Asignatura'
    ],
    model: [
        'Asignatura'
    ],
    init: function () {
        this.control({
            'asignaturas': {
                itemdblclick: this.editarAsignatura
            },
            'editarAsignatura button[action=save]': {
                click: this.actualizarAsignatura
            }
        });
    },
    editarAsignatura: function (grid, record) {
        var view = Ext.widget('editarAsignatura');
        view.down('form').loadRecord(record);
    },
    actualizarAsignatura: function (button) {
        var win = button.up('window'),
                form = win.down('form'),
                record = form.getRecord(),
                values = form.getValues();

        record.set(values);
        win.close();
    }
});
Ext.define('asistencia.controller.Persona', {
    extend: 'Ext.app.Controller',
    views: [
        'persona.Listado',
        'persona.Editar'
    ],
    stores: [
        'Persona'
    ],
    model: [
        'Persona'
    ],
    init: function () {
        this.control({
            'personas': {
                itemdblclick: this.editarPersona
            },
            'editarPersona button[action=save]': {
                click: this.actualizarPersona
            }
        });
    },
    editarPersona: function (grid, record) {
        var view = Ext.widget('editarPersona');
        view.down('form').loadRecord(record);
    },
    actualizarPersona: function (button) {
        var win = button.up('window'),
                form = win.down('form'),
                record = form.getRecord(),
                values = form.getValues();

        record.set(values);
        win.close();
    }
});
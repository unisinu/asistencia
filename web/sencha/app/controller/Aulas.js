Ext.define('asistencia.controller.Aulas', {
    extend: 'Ext.app.Controller',
    views: [
        'aulas.Listado',
        'aulas.Editar',
    ],
    stores: [
        'Aulas'
    ],
    model: [
        'Aulas'
    ],
    init: function () {
        this.control({
            'aulas': {
                itemdblclick: this.editarAulas
            }
        });
    },
    editarAulas: function (grid, record) {
        var view = Ext.widget('editarAulas');
        view.down('form').loadRecord(record);
    }
});



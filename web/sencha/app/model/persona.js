Ext.define('asistencia.model.Persona', {
    extend: 'Ext.data.Model',
    fields: ['codigo', 'nombre','apellido','email','telefono']
});

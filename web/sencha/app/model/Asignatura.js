Ext.define('asistencia.model.Asignatura', {
    extend: 'Ext.data.Model',
    fields: ['nombre', 'semestre']
});
Ext.define('asistencia.model.Grupo', {
    extend: 'Ext.data.Model',
    fields: ['id','nombre', 'semestre','Aula_id','Asignatura_id','Asistencia_id']
});
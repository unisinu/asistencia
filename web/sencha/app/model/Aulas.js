Ext.define('asistencia.model.Aulas', {
    extend: 'Ext.data.Model',
    fields: ['id', 'nombre']
});
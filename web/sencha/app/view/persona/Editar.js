Ext.define('asistencia.view.persona.Editar', {
    extend: 'Ext.window.Window',
    alias: 'widget.editarPersona',

    title: 'Editar Persona',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'codigo',
                        fieldLabel: 'codigo'
                    },
                    {
                        xtype: 'textfield',
                        name : 'nombre',
                        fieldLabel: 'Nombre'
                    },
                    {
                        xtype: 'textfield',
                        name : 'apellido',
                        fieldLabel: 'apellido'
                    },
                    {
                        xtype: 'textfield',
                        name : 'email',
                        fieldLabel: 'email'
                    },
                    {
                        xtype: 'textfield',
                        name : 'telefono',
                        fieldLabel: 'telefono'
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Guardar',
                action: 'save'
            },
            {
                text: 'Cancelar',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});
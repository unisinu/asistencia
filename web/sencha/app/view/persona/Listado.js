Ext.define('asistencia.view.asignatura.Listado' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.personas',

    title: 'Personas',

    initComponent: function() {
        this.store = 'Persona',
        this.columns = [
            {header: 'codigo',  dataIndex: 'codigo',  flex: 1},
            {header: 'Nombre',  dataIndex: 'nombre',  flex: 1},
            {header: 'apellido', dataIndex: 'apellido', flex: 1},
            {header: 'email',  dataIndex: 'email',  flex: 1},
            {header: 'telefono',  dataIndex: 'telefono',  flex: 1}
        ];

        this.callParent(arguments);
    }
});
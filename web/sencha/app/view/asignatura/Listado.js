Ext.define('asistencia.view.asignatura.Listado' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.asignaturas',

    title: 'Asignaturas',

    initComponent: function() {
        this.store = 'Asignatura',
        this.columns = [
            {header: 'Nombre',  dataIndex: 'nombre',  flex: 1},
            {header: 'Semestre', dataIndex: 'semestre', flex: 1}
        ];

        this.callParent(arguments);
    }
});
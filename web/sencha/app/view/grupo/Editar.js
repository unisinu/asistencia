Ext.define('asistencia.view.grupo.Editar', {
    extend: 'Ext.window.Window',
    alias: 'widget.editarGrupo',

    title: 'Editar Grupo',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'nombre',
                        fieldLabel: 'Nombre'
                    },
                    {
                        xtype: 'textfield',
                        name : 'semestre',
                        fieldLabel: 'Semestre'
                    },{
                        xtype:'combo',
                        name : 'Aula_id',
                        fieldLabel: 'Aula'
                        
                    },{
                        xtype:'combo',
                        name : 'Asignatura_id',
                        fieldLabel: 'Asignatura'
                        
                    },{
                        xtype:'combo',
                        name : 'Asistencia_id',
                        fieldLabel: 'Asistencia'
                        
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Guardar',
                action: 'save'
            },
            {
                text: 'Cancelar',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});
Ext.define('asistencia.view.grupo.Listado' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.grupo',

    title: 'Grupos',

    initComponent: function() {
        this.store = 'Grupo',
        this.columns = [
            {header: 'Nombre',  dataIndex: 'nombre',  flex: 1},
            {header: 'Semestre', dataIndex: 'semestre', flex: 1},
            {header: 'Aula', dataIndex: 'Aula_id', flex: 1},
            {header: 'Asignatura', dataIndex: 'Asignatura_id', flex: 1},
            {header: 'Asitencia', dataIndex: 'Asistencia_id', flex: 1}
        ];

        this.callParent(arguments);
    }
});
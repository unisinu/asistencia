Ext.define('asistencia.view.aulas.Listado' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.aulas',

    title: 'Aulas',

    initComponent: function() {
        this.store = 'Aulas',
        this.columns = [
            {header: 'Id',  dataIndex: 'id',  flex: 1},
            {header: 'Nombre', dataIndex: 'nombre', flex: 1}
        ];

        this.callParent(arguments);
    }
});
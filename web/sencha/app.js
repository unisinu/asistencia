Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'asistencia',
    appFolder: 'app',
    controllers: [
        'Asignatura'
    ],
    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [
                {
                    xtype: 'asignaturas',
                    title: 'Asignaturas',
                    html: 'List of users will go here'
                }
            ]
        });
    }
});
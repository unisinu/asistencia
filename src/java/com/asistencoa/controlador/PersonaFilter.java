/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asistencoa.controlador;
import com.asistencia.modelo.bean.Persona;
import com.asistencia.modelo.dao.PersonaDAO;
import java.io.StringWriter;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

@WebFilter(filterName = "PersonaFilter", urlPatterns = {"/sencha/personas"})
public class PersonaFilter extends BaseFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        try {

            PersonaDAO dao = (PersonaDAO) spring.getBean("personaDAO");
            List<Persona> personas = dao.findPersona();
            StringWriter out = new StringWriter();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jg = jsonFactory.createJsonGenerator(out);
            jg.useDefaultPrettyPrinter();
//            jg.writeStartObject();
//            jg.writeFieldName("asignaturas");
            jg.writeStartArray();
            for(Persona persona : personas) {
                jg.writeStartObject();
                jg.writeStringField("nombre", persona.getNombre());
                jg.writeStringField("apellido", persona.getApellido());
                jg.writeStringField("codigo", persona.getCodigo());
                jg.writeEndObject();
            }            
            jg.writeEndArray();
//            jg.writeEndObject();
            jg.close();
            response.setContentType("text/json");
            response.getOutputStream().println(out.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

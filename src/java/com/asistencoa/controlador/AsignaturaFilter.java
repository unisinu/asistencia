package com.asistencoa.controlador;

import com.asistencia.modelo.bean.Asignatura;
import com.asistencia.modelo.dao.AsignaturaDAO;
import java.io.StringWriter;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

@WebFilter(filterName = "AsignaturaFilter", urlPatterns = {"/sencha/asignaturas"})
public class AsignaturaFilter extends BaseFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        try {

            AsignaturaDAO dao = (AsignaturaDAO) spring.getBean("asignaturaDAO");
            List<Asignatura> asignaturas = dao.findAsignaturas();
            StringWriter out = new StringWriter();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jg = jsonFactory.createJsonGenerator(out);
            jg.useDefaultPrettyPrinter();
//            jg.writeStartObject();
//            jg.writeFieldName("asignaturas");
            jg.writeStartArray();
            for(Asignatura asignatura : asignaturas) {
                jg.writeStartObject();
                jg.writeStringField("nombre", asignatura.getNombre());
                jg.writeStringField("codigo", asignatura.getCodigo());
                jg.writeEndObject();
            }            
            jg.writeEndArray();
//            jg.writeEndObject();
            jg.close();
            response.setContentType("text/json");
            response.getOutputStream().println(out.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

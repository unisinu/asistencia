/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asistencoa.controlador;

import java.io.StringWriter;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

/**
 *
 * @author saladesarrollo
 */

@WebFilter(filterName = "UserFilter", urlPatterns = {"/sencha/user"})
public class UserFilter extends BaseFilter  {
    
    

    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        
        //Verificar filtros
        
        try {
            StringWriter out = new StringWriter();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jg = jsonFactory.createJsonGenerator(out);
            jg.useDefaultPrettyPrinter();
            jg.writeStartObject();
            jg.writeFieldName("usuarios");
            jg.writeStartArray();
            jg.writeStartObject();
            jg.writeStringField("username", "luisgtr100");
            jg.writeStringField("password", "312164");
            jg.writeEndObject();
            jg.writeStartObject();
            jg.writeStringField("username", "vanemejia");
            jg.writeStringField("password", "123456");
            jg.writeEndObject();
            jg.writeEndArray();
            jg.writeEndObject();
            jg.close();
            response.setContentType("text/json");
            response.getOutputStream().println(out.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}

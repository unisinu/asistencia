package com.asistencoa.controlador;

import com.asistencia.modelo.bean.Computador;
import com.asistencia.modelo.bean.Grupo;
import com.asistencia.modelo.dao.ComputadorDAO;
import java.io.StringWriter;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 *
 * @author Hugo
 */
@WebFilter(filterName = "ComputadorFilter", urlPatterns = {"/angular/computador"})
public class ComputadorFilter extends BaseFilter {

    private XmlWebApplicationContext spring;

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        if (spring == null) {
            spring = (XmlWebApplicationContext) WebApplicationContextUtils
                    .getWebApplicationContext(request.getServletContext());
        }
        try {
            ComputadorDAO dao = (ComputadorDAO) spring.getBean("computadorDAO");
            List<Computador> computadores = dao.findComputadores();
            StringWriter out = new StringWriter();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jg = jsonFactory.createJsonGenerator(out);
            jg.useDefaultPrettyPrinter();
//            jg.writeStartObject(); no funciona con angularjs
//            jg.writeFieldName("grupos"); no funciona con angularjs
            jg.writeStartArray();

            if (computadores != null && !computadores.isEmpty()) {
                for (Computador computador : computadores) {
                    jg.writeStartObject();
                    jg.writeStringField("marca", computador.getMarca());
                    jg.writeStringField("procesador", computador.getProcesaor());
                    jg.writeNumberField("ram", computador.getRam());
                    jg.writeEndObject();
                }
            }
            jg.writeEndArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

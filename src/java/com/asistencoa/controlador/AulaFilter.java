/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asistencoa.controlador;
import com.asistencia.modelo.bean.Aula;
import com.asistencia.modelo.dao.AulaDao;
import java.io.StringWriter;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

@WebFilter(filterName = "AulaFilter", urlPatterns = {"/sencha/aulas"})
public class AulaFilter extends BaseFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        try {

            AulaDao dao = (AulaDao) spring.getBean("aulaDAO");
            List<Aula> aulas = dao.findAula();
            StringWriter out = new StringWriter();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jg = jsonFactory.createJsonGenerator(out);
            jg.useDefaultPrettyPrinter();

            jg.writeStartArray();
            for(Aula aula : aulas) {
                jg.writeStartObject();
                jg.writeStringField("nombre", aula.getNombre());
              
                jg.writeEndObject();
            }            
            jg.writeEndArray();

            jg.close();
            response.setContentType("text/json");
            response.getOutputStream().println(out.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

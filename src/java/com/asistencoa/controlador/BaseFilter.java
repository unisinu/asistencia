package com.asistencoa.controlador;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 *
 * @author Hugo
 */
public abstract class BaseFilter implements Filter {

    private static final boolean debug = false;
    protected XmlWebApplicationContext spring;
    private FilterConfig filterConfig = null;

    public BaseFilter() {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        if (spring == null) {
            spring = (XmlWebApplicationContext) WebApplicationContextUtils
                    .getWebApplicationContext(req.getServletContext());
        }
        doFilter(req, res, chain);
    }

    public abstract void doFilter(HttpServletRequest request, HttpServletResponse response,
            FilterChain chain);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}

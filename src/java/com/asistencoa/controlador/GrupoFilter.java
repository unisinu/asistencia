package com.asistencoa.controlador;

import com.asistencia.modelo.bean.Asignatura;
import com.asistencia.modelo.bean.Grupo;
import com.asistencia.modelo.dao.AsignaturaDAO;
import com.asistencia.modelo.dao.GrupoDAO;
import java.io.StringWriter;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
/**
 *
 * @author Hugo
 */
@WebFilter(filterName = "GrupoFilter", urlPatterns = {"/angular/grupo"})
public class GrupoFilter extends BaseFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        try {

            GrupoDAO dao = (GrupoDAO) spring.getBean("grupoDAO");
            List<Grupo> grupos = dao.findGrupos();
            StringWriter out = new StringWriter();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jg = jsonFactory.createJsonGenerator(out);
            jg.useDefaultPrettyPrinter();
//            jg.writeStartObject();
//            jg.writeFieldName("asignaturas");
            jg.writeStartArray();
            for(Grupo grupo : grupos) {
                jg.writeStartObject();
                jg.writeStringField("Nombre", grupo.getNombre());
                jg.writeStringField("Semestre", grupo.getSemestre()+"");
                jg.writeStringField("Aula", grupo.getAula()+"");
                jg.writeEndObject();
            }            
            jg.writeEndArray();
//            jg.writeEndObject();
            jg.close();
            response.setContentType("text/json");
            response.getOutputStream().println(out.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    }


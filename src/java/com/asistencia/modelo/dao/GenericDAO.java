package com.asistencia.modelo.dao;

import java.io.Serializable;

/**
 *
 * @author Hugo
 */
public interface GenericDAO<T, PK extends Serializable> {
    T create(T t);
    T read(PK id);
    T update(T t);
    void delete(T t);
}

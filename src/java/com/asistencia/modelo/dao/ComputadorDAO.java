package com.asistencia.modelo.dao;

import com.asistencia.modelo.bean.Computador;
import com.asistencia.modelo.bean.Grupo;
import com.asistencia.modelo.dao.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class ComputadorDAO implements Serializable {

    private EntityManagerFactory emf = null;

    public ComputadorDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Computador computador) throws RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(computador);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Computador> findComputadores() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT c FROM Computador c"); 
            return q.getResultList();
        } finally {
            em.close();
        }
    }
}

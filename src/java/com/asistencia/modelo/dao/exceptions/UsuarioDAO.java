/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.asistencia.modelo.dao.exceptions;

import com.asistencia.modelo.bean.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

/**
 *
 * @author saladesarrollo
 */
public class UsuarioDAO implements Serializable{
    
    public UsuarioDAO (UserTransaction utx, EntityManagerFactory emf){
    
        this.utx = utx;
        this.emf = emf;
        
    }
    
    private UserTransaction utx;
    private EntityManagerFactory emf;
    
    public EntityManager getEntityManager(){
       return emf.createEntityManager();
    }
    
    public void create(Usuario usuario) throws RollbackFailureException, Exception{
    
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(usuario);
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw e;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        }
    
    
    
        public void edit(Usuario usuario) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            usuario = em.merge(usuario);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = usuario.getId();
                if (findAsignatura(id) == null) {
                    throw new NonexistentEntityException("The asignatura with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
        
        
        
            public List<Usuario> findAsignaturas() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT username  FROM Usuario username");

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findAsignatura(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }
    
    
    
    }



    

